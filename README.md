# ethernet-acquisition

## Features
 - Xilinx Artix 7 FPGA, option of XC7A15T - XC7A100T
 - 10/100/1000 Ethernet with Microchip KSZ9031 PHY
 - 125 MHz oscillator
 - 4 expansion connectors with 20 1.8 V IO each, 1.8 and 3.7 V provided
 - SPI configuration flash
 - designed with Kicad